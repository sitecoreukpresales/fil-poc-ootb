﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;

namespace Demo.FIL.WebUI.Models
{
    [SitecoreType(AutoMap = true)]
    public class Tag
    {
        [SitecoreId]
        public virtual Guid Id { get; set; }
        public virtual Link Link { get; set; }
        public virtual Image Image { get; set; }
        public virtual string Text { get; set; }
    }
}