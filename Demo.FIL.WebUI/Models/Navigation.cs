﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;

namespace Demo.FIL.WebUI.Models
{   
    public class Navigation
    {
        [SitecoreId]
        public virtual Guid Id { get; set; }

        [SitecoreChildren]
        [SitecoreField(Setting = SitecoreFieldSettings.InferType)]
        public virtual IEnumerable<NavigationSection> Children { get; set; }
    }
}