﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Configuration.Fluent;
using Glass.Mapper.Sc.Fields;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;

namespace Demo.FIL.WebUI.Models
{
    [SitecoreType(AutoMap = true)]
    public class BlockQuote
    {
        [SitecoreId]
        public virtual Guid Id { get; set; }
        public virtual string Text { get; set; }
    }
}