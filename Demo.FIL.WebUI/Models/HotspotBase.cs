﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;

namespace Demo.FIL.WebUI.Models
{
    [SitecoreType(TemplateId = "{44CE85A5-6156-402C-AA08-012E09F7213F}", AutoMap = true)]
    public class HotspotBase
    {
        [SitecoreId]
        public virtual Guid Id { get; set; }

        public virtual Link Link { get; set; }
    }
}