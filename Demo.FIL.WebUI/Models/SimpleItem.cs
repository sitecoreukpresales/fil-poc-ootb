﻿using System.Collections.Generic;
using System.Linq;
using Demo.FIL.WebUI.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Links;

namespace Demo.FIL.WebUI.Models
{
    public class SimpleItem : CustomItem
    {
        public SimpleItem(Item item) : base(item)
        {
            Assert.IsNotNull(item, "item");
        }


        public string TemplateName
        {
            get { return InnerItem.TemplateName.ToLower(); }
        }

        public string Url
        {
            get { return LinkManager.GetItemUrl(InnerItem); }
        }

        public bool ShowInMenu
        {
            get{
                 return InnerItem[FieldId.ShowItemInMenu] == "1" ? true : false; 
            }
        }

        public string MenuTitleField
        {
            get
            {
                if (! string.IsNullOrEmpty(InnerItem["menu title"]))
                {
                    return "menu title";
                }
                else
                {
                    return "name";
                }
            }
        }


        public string SearchDescription
        {
            get { return SiteConfiguration.GetPageDescripton(Item); }
        }

        /// <summary>
        /// If item has a tag field return all selected values from the list
        /// </summary>
        public IEnumerable<string> TagValues
        {
            get
            {
                if (InnerItem.Fields["Tags"] != null)
                {
                    return  ((MultilistField)InnerItem.Fields["Tags"]).GetItems().Select(x => x.Name);
                }
                else
                {
                    return Enumerable.Empty<string>();
                }
            }
        }

        public Item Item
        {
            get { return InnerItem; }
        }

        public IEnumerable<SimpleItem> ChildrenInCurrentLanguage
        {
            get
            {
                return Children.Where(x => SiteConfiguration.DoesItemExistInCurrentLanguage(x.Item));
            }
        }

        public IEnumerable<SimpleItem> Children
        {
            get
            {
                return InnerItem.Children.Select(x => new SimpleItem(x));
            }
        }

        public static class FieldId
        {
            public static readonly ID ShowItemInMenu = new ID("{7E003143-26F2-4A28-B70A-08548286F6BA}");
        }


        
    }
}

