﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Configuration.Fluent;
using Glass.Mapper.Sc.Fields;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;

namespace Demo.FIL.WebUI.Models
{
    [SitecoreType(TemplateId = "{A418635C-FBF6-4C03-B9C8-1B639269B04A}", AutoMap = true)]
    public class Author
    {
        [SitecoreId]
        public virtual Guid Id { get; set; }
        [SitecoreField("Author Name")]
        public virtual string AuthorName { get; set; }
        public virtual Image Image { get; set; }
        public virtual string Bio { get; set; }
    }
}