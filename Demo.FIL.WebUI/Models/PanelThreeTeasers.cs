﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Glass.Mapper.Sc.Configuration.Attributes;
using Sitecore.Data.Items;

namespace Demo.FIL.WebUI.Models
{

    public class PanelThreeTeasers
    {

        [SitecoreId]
        public virtual Guid Id { get; set; }

        public virtual IEnumerable<HotspotImage> Teasers { get; set; }
    }
}