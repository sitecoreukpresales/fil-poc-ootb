﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;

namespace Demo.FIL.WebUI.Models
{
    [SitecoreType(AutoMap = true)]
    public class NavigationSection
    {
        [SitecoreId]
        public virtual Guid Id { get; set; }

        [SitecoreInfo(SitecoreInfoType.Url)]
        public virtual string Url { get; set; }

        [SitecoreInfo(SitecoreInfoType.Name)]
        public virtual string Name { get; set; }

        [SitecoreField("NavigationText")]
        public virtual string NavigationText { get; set; }
        [SitecoreField("Display In Navigation")]
        public virtual bool DisplayInNav { get; set; }

        [SitecoreChildren]
        public virtual IEnumerable<NavigationSection> Children { get; set; }
    }
}