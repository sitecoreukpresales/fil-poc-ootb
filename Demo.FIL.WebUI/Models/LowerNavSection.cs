﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;

namespace Demo.FIL.WebUI.Models
{
    [SitecoreType(TemplateId = "{2D918E7A-424F-410D-B21D-2687D66B63AD}", EnforceTemplate = SitecoreEnforceTemplate.Template)]
    public class LowerNavigationSection : NavigationSection
    {
    }
}