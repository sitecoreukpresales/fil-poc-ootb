﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;

namespace Demo.FIL.WebUI.Models
{
    [SitecoreType(TemplateId = "{790625AA-B85A-4867-A3C1-4775BDEA31EA}", AutoMap = true)]
    public class ContactSpot
    {
        [SitecoreId]
        public virtual Guid Id { get; set; }

        public virtual string Title { get; set; }

        public virtual string Details { get; set; }
    }
}