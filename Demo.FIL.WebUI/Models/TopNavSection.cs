﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;

namespace Demo.FIL.WebUI.Models
{
    [SitecoreType(TemplateId = "{0E29C606-6A48-4FF6-8930-D03604D9F520}", EnforceTemplate = SitecoreEnforceTemplate.Template)]
    public class TopNavigationSection : NavigationSection
    {
    }
}