﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;

namespace Demo.FIL.WebUI.Models
{
    public class ContactSpotList
    {
        [SitecoreId]
        public virtual Guid Id { get; set; }
        public virtual string Title { get; set; }

        public virtual string Summary { get; set; }

        public virtual IList<ContactSpot> contactSpots { get; set; }

    }
}