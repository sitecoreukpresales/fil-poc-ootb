﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;

namespace Demo.FIL.WebUI.Models
{
    [SitecoreType(AutoMap = true)]
    public class Footer
    {
        [SitecoreId]
        public virtual Guid Id { get; set; }
        public virtual string CopyRight { get; set; }
        public virtual Image Logo { get; set; }
        public virtual IEnumerable<MyLink> Links { get; set; }
    }
}