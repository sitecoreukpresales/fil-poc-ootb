﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;

namespace Demo.FIL.WebUI.Models
{
    [SitecoreType(AutoMap = true)]
    public class Tags
    {
        [SitecoreId]
        public virtual Guid Id { get; set; }
        [SitecoreField(FieldName = "Topic")]
        public virtual IList<Tag> TopicTags { get; set; }
        [SitecoreField(FieldName = "Region")]
        public virtual IList<Tag> RegionTags { get; set; }
        [SitecoreField(FieldName = "ContentType")]
        public virtual IList<Tag> ContentTypeTags { get; set; }
    }
}