﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;

namespace Demo.FIL.WebUI.Models
{

    public class Panel
    {
        [SitecoreId]
        public virtual Guid Id { get; set; }
        public virtual string PanelColour { get; set; }
        public virtual Image Image { get; set; }
        
    }
}