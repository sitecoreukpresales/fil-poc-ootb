﻿using System;
using System.Collections.Generic;
using System.Linq;
using Demo.FIL.WebUI.Configuration;
using Demo.FIL.WebUI.Configuration.Search.Models;
using Sitecore.ContentSearch.Linq;
using Sitecore.Data.Items;

namespace Demo.FIL.WebUI.Models
{
    public class SearchResults
    {

        public string SearchString { get; set; }

        public string[] SelectedFacets
        {
            get { return null; }
        }

        public List<SimpleItem> Results { get; set; }

        public List<Facet> Facets { get; set; }

        public SearchResults(string searchStr, string[] facets)
        {
            // if we don't have a searchStr for some reason...
            if (searchStr == string.Empty) searchStr = "*";

            using (var context = SiteConfiguration.GetSearchContext(Sitecore.Context.Item))
            {
                // Start the search query building
                var query = context.GetQueryable<SitecoreItem>().Where(item => item.Path.StartsWith(Sitecore.Context.Site.StartPath));

                //query = query.Where(item => item.TemplateName == "Job Function");

                // we will split the spaces and require all words to be in the index.
                foreach (string word in searchStr.Split(' '))
                {
                    query = query.Where(item => item.Title.Contains(word) || item.Content.Contains(word) || item.JobTitle.Contains(word) || item.Description.Contains(word));
                }

                if (facets != null)
                {
                    foreach (string facet in facets)
                    {
                        if (facet.Contains("|"))
                        {
                            string[] values = facet.Split('|');
                            string val = values[1];
                            if (values[0] == "Type") { query = query.Where(item => item.TemplateName.Equals(val)); }
                            if (values[0] == "Tags") { query = query.Where(item => item.Tags.Equals(val)); }
                            if (values[0] == "Location") { query = query.Where(item => item.Location.Equals(val)); }
                            if (values[0] == "Condition") { query = query.Where(item => item.Conditions.Equals(val)); }
                            //     if (values[0] == "Type") { query = query.Where(item => item.Type.Equals(val)); }


                        }

                    }
                }

                var results = query
                    .Filter(item => item.Language == Sitecore.Context.Language.Name)
                    .FacetOn(item => item.Tags)
                    .FacetOn(item => item.Conditions)
                    .FacetOn(item => item.Location)
                    .FacetOn(item => item.TemplateName)
                    .GetResults();

                this.Results = new List<SimpleItem>();
                this.Facets = new List<Facet>();

                this.SearchString = searchStr;
                foreach (SearchHit<SitecoreItem> result in results.Hits)
                {
                    var item = result.Document.GetItem();
                    if (item != null)
                    {
                        this.Results.Add(new SimpleItem(item));
                    }
                }

                foreach (FacetCategory fc in results.Facets.Categories)
                {
                    Facet f = new Facet();
                    f.Items = new List<FacetItem>();

                    if (fc.Name == "_templatename")
                    {
                        f.FacetName = SiteConfiguration.GetDictionaryText("Type");
                        foreach (var a in fc.Values)
                        {
                            f.Items.Add(new FacetItem(a.Name, String.Format("{0} ({1})", a.Name, a.AggregateCount), false));
                        }
                    }

                    if (fc.Name == "type")
                    {
                        f.FacetName = "Type";
                        foreach (var a in fc.Values)
                        {
                            f.Items.Add(new FacetItem(a.Name, String.Format("{0} ({1})", a.Name, a.AggregateCount), false));
                        }
                    }
                    if (fc.Name == "conditions")
                    {
                        f.FacetName = "Condition";
                        foreach (var a in fc.Values)
                        {
                            f.Items.Add(new FacetItem(a.Name, String.Format("{0} ({1})", a.Name, a.AggregateCount), false));
                        }
                    }
                    if (fc.Name == "location")
                    {
                        f.FacetName = "Location";
                        var n = string.Empty;
                        foreach (var a in fc.Values)
                        {
                            if (a.AggregateCount > 1)
                            {
                                n = a.Name;
                                if (n.Length > 21) n = String.Format("{0}..", n.Substring(0, 21));
                                f.Items.Add(new FacetItem(a.Name, String.Format("{0} ({1})", n, a.AggregateCount), false));
                            }
                        }
                    }

                    if (fc.Name == "__semantics")
                    {
                        f.FacetName = SiteConfiguration.GetDictionaryText("Tags");
                        foreach (var a in fc.Values)
                        {
                            Item tag = Sitecore.Context.Database.GetItem(new Sitecore.Data.ID(a.Name));
                            f.Items.Add(new FacetItem(a.Name, String.Format("{0} ({1})", tag.Name, a.AggregateCount), false));
                        }
                    }



                    // set the correct facet entries to selected
                    if (facets != null)
                    {
                        foreach (string facet in facets)
                        {
                            if (facet.Contains("|"))
                            {
                                string[] values = facet.Split('|');
                                if (values[0] == f.FacetName)
                                {
                                    foreach (FacetItem fi in f.Items) { if (fi.Id == values[1]) fi.Selected = true; }
                                }
                            }
                        }
                    }

                    if (f.Items.Count > 0) this.Facets.Add(f);
                }
            }
        }
    

        public class Facet
        {
            public Facet() { }
            public string FacetName { get; set; }
            public List<FacetItem> Items { get; set; }
        }

        public class FacetItem
        {
            public FacetItem() { }
            public FacetItem(string id, string name, bool selected)
            {
                Id = id;
                Name = name;
                Selected = selected;
            }
            public string Id { get; set; }
            public string Name { get; set; }
            public bool Selected { get; set; }
        }
    }
}