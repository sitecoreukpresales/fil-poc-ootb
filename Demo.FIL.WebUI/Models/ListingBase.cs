﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Sitecore.Data.Fields;

namespace Demo.FIL.WebUI.Models
{
    [SitecoreType(AutoMap = true, TemplateId = "{36070C2C-F61A-4049-81D5-B49260F36BCF}")]
    public class ListingBase
    {
        [SitecoreId]
        public virtual Guid Id { get; set; }
        public virtual string IntroText { get; set; }
        [SitecoreField(FieldName = "Topic")]
        public virtual IList<Tag> TopicTags { get; set; }
        [SitecoreField(FieldName = "Region")]
        public virtual IList<Tag> RegionTags { get; set; }
        [SitecoreField(FieldName = "ContentType")]
        public virtual IList<Tag> ContentTypeTags { get; set; }
        //public virtual Tags Tags { get; set; }
        public virtual IList<Author> Author { get; set; }
        public virtual DateTime Date { get; set; }
        public virtual Link Link { get; set; }
        public virtual string Title { get; set; }
    }
}