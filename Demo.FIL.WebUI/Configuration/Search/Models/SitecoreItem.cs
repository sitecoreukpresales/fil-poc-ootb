﻿using System;
using System.Runtime.CompilerServices;
using Sitecore.ContentSearch;
using Sitecore.ContentSearch.SearchTypes;
using Sitecore.Data.Items;

namespace Demo.FIL.WebUI.Configuration.Search.Models
{
    public sealed class SitecoreItem : SearchResultItem
    {
        //private ItemVisualization IV;
        //public bool HasLayout { get; set; }

        public string Title { get; set; }

        [IndexField("__smallcreateddate")]
        public DateTime PublishDate { get; set; }

        //[IndexField("has_presentation")]
        //public bool HasPresentation { get; set; }

        [IndexField(BuiltinFields.Semantics)]
        public string Tags { get; set; }

        [IndexField("show_in_search_results")]
        public bool ShowInSearchResults { get; set; }

        [IndexField("conditions")]
        public string Conditions { get; set; }

        [IndexField("location")]
        public string Location { get; set; }

        [IndexField("type")]
        public string Type { get; set; }

        [IndexField("jobtitle")]
        public string JobTitle { get; set; }

        [IndexField("description")]
        public string Description { get; set; }

        //public SitecoreItem()
        //{
        //    IV = new ItemVisualization(GetItem());
        //    HasLayout = IV.Layout != null;
        //}
    }
}