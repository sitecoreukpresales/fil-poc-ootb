﻿using System.Collections.Generic;
using Sitecore.Data.Items;

namespace Demo.FIL.WebUI.Configuration.Search.Helper
{
    public class ItemIDComparer : IEqualityComparer<Item>
    {
        public bool Equals(Item x, Item y)
        {
            return x.ID == y.ID;
        }

        public int GetHashCode(Item obj)
        {
            return obj.ID.GetHashCode();
        }
    }
}