﻿using System.Collections.Generic;
using System.Linq;

namespace Demo.FIL.WebUI.Configuration.Search.Helper
{
    public static class IEnumerableExtensions
    {
        public static string Join<T>(this IEnumerable<T> target, string separator)
        {
            return target == null
                ? string.Empty
                : string.Join(separator, target.Where(value => value != null).Select(value => value.ToString()).ToArray());
        }
    }
}
