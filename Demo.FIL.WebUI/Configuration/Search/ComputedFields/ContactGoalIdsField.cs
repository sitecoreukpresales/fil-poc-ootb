﻿using System;
using System.Linq;
using Sitecore.Analytics.Data;
using Sitecore.ContentSearch;
using Sitecore.ContentSearch.ComputedFields;
using Demo.FIL.WebUI.Configuration.Search.Helper;

namespace Demo.FIL.WebUI.Configuration.Search.ComputedFields
{
    public class ContactGoalIdsField : AbstractComputedIndexField, ISearchIndexInitializable
    {
        private ISearchIndex index;

        public ContactGoalIdsField() { }

        public ContactGoalIdsField(System.Xml.XmlNode configNode)
            : this() { }



        public override object ComputeFieldValue(IIndexable indexable)
        {
            if (indexable.GetFieldByName("type") == null)
            {
                return null;
            }

            if (indexable.GetFieldByName("type").Value.ToString() != "contact")
            {
                // Only process contacts
                return null;
            }

            var contactId = new Guid(indexable.GetFieldByName("contact.ContactId").Value.ToString());

            var repository = new ContactRepository();

            var interactions = repository.LoadHistoricalInteractions(contactId, int.MaxValue, null, null);

            // Tracker.Current.Interaction.GetPages().SelectMany(page => page.PageEvents.Where(y => y.IsGoal)).ToList().Take(5);


            var ap = interactions.SelectMany(p => p.Pages);
            var goals = ap.SelectMany(p => p.PageEvents.Where(y => y.IsGoal)).ToArray();
            var goalIds = goals.Select(g => g.PageEventDefinitionId.ToString().Replace("-", string.Empty)).Distinct();
            return goals.Length == 0 ? "_NONE_" : goalIds.Join(" ");
        }

        public void Initialize(ISearchIndex searchIndex)
        {
            index = searchIndex;
        }
    }
}
