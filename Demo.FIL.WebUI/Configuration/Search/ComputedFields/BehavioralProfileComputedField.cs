﻿using System;
using System.Linq;
using Sitecore.Analytics.Data;
using Sitecore.ContentSearch;
using Sitecore.ContentSearch.ComputedFields;

namespace Demo.FIL.WebUI.Configuration.Search.ComputedFields
{
    public class BehavioralProfileComputedField : AbstractComputedIndexField, ISearchIndexInitializable
    {
        private ISearchIndex index;

        public BehavioralProfileComputedField() { }

        public BehavioralProfileComputedField(System.Xml.XmlNode configNode)
            : this() { }

        public override object ComputeFieldValue(IIndexable indexable)
        {
            if (indexable.GetFieldByName("type") == null)
            {
                return null;
            }

            if (indexable.GetFieldByName("type").Value.ToString() != "contact")
            {
                // Only process contacts
                return null;
            }

            var contactId = new Guid(indexable.GetFieldByName("contact.ContactId").Value.ToString());
            var repository = new ContactRepository();
            var contact = repository.LoadContactReadOnly(contactId);// LoadHistoricalInteractions(contactId, int.MaxValue, null, null);

            string result = string.Empty;
            foreach (var profile in contact.BehaviorProfiles.Profiles)
            {
                result += profile.PatternId.ToString().Replace("-", string.Empty) + " ";
            }

            return contact.BehaviorProfiles.Profiles.Count() == 0 ? null : result.Replace("{", "").Replace("}", "").Trim();
        }



        public void Initialize(ISearchIndex searchIndex)
        {
            index = searchIndex;
        }

    }
}

