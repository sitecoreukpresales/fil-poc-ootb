﻿using System;
using Sitecore.Analytics.Data;
using Sitecore.Analytics.Model.Entities;
using Sitecore.ContentSearch;
using Sitecore.ContentSearch.ComputedFields;

namespace Demo.FIL.WebUI.Configuration.Search.ComputedFields
{
    public class ContactAgeRange : AbstractComputedIndexField, ISearchIndexInitializable
    {
        private ISearchIndex index;

        public ContactAgeRange() { }

        public ContactAgeRange(System.Xml.XmlNode configNode)
            : this() { }



        public override object ComputeFieldValue(IIndexable indexable)
        {
            if (indexable.GetFieldByName("type") == null)
            {
                return null;
            }

            if (indexable.GetFieldByName("type").Value.ToString() != "contact")
            {
                // Only process contacts
                return null;
            }

            var contactId = new Guid(indexable.GetFieldByName("contact.ContactId").Value.ToString());

            var repository = new ContactRepository();

            var contact = repository.LoadContactReadOnly(contactId);
            var personalInfo = contact.GetFacet<IContactPersonalInfo>("Personal");

            if (!string.IsNullOrEmpty(personalInfo.BirthDate.ToString()))
            {
                int age = new DateTime(DateTime.Now.Subtract(personalInfo.BirthDate.Value).Ticks).Year - 1;
                if (age < 19)
                {
                    return "under18";
                }
                if (age < 31)
                {
                    return "19to31";
                }
                if (age < 41)
                {
                    return "31to40";
                }
                if (age < 61)
                {
                    return "41to60";
                }
                if (age < 1000)
                {
                    return "over60";
                }

                return null;
            }
            else
            {
                return null;
            }
        }

        public void Initialize(ISearchIndex searchIndex)
        {
            index = searchIndex;
        }
    }
}
