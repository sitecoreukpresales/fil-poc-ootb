﻿using System;
using System.Collections.Generic;
using Sitecore.Analytics.Data;
using Sitecore.ContentSearch;
using Sitecore.ContentSearch.ComputedFields;
using Sitecore.Data;
using Sitecore.Data.Items;

namespace Demo.FIL.WebUI.Configuration.Search.ComputedFields
{
    public class ProfileCardComputedField : IComputedIndexField
    {
        public string FieldName { get; set; }
        public string ReturnType { get; set; }
        public object ComputeFieldValue(IIndexable indexable)
        {
            Item i = ((Item)(indexable as SitecoreIndexableItem));
            if (i != null && i["__Tracking"] != String.Empty)
            {
                TrackingField field = new TrackingField(i.Fields["__Tracking"]);
                ContentProfile[] profiles = field.Profiles;

                List<ID> presets = new List<ID>();
                foreach (ContentProfile profile in profiles)
                {
                    if (profile.Presets != null)
                    {
                        foreach (var a in profile.Presets)
                        {
                            foreach (Item card in profile.GetProfileItem().Axes.GetDescendants())
                            {
                                if (card.Key == a.Key && card.Template.Key.StartsWith("profile card"))
                                    presets.Add(card.ID);
                            }
                        }
                    }
                }
                return presets;
            }
            return string.Empty;
        }
    }
}