﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Links;

namespace Demo.FIL.WebUI.Configuration
{
    public static class UIHelpers
    {
        /// <summary>
        /// Wraps the standard sitecore dictionary call.  In order to have all site labels and standard text managed within the CMS, we use the dictionary.
        /// These items simply hold the value for each label.    
        /// </summary>
        /// <param name="key">The dictionary key you are requesting.</param>
        /// <returns>The phrase value for the requested key.</returns>
        public static string GetDictionaryText(string key)
        {
            return Sitecore.Globalization.Translate.Text(key);
        }

        /// <summary>
        /// Provides the ablility to get a link to the target item of a guid type such as droptree in Sitecore.  
        /// /// </summary>
        /// <param name="item">The item that has the droptree.</param>
        /// <param name="fieldName">The name of the drop tree field.</param>
        /// <returns>the link to the item specified in the droptree</returns>
        public static string GetLink(this HtmlHelper htmlHelper, Item item, string fieldName)
        {
            return GetLink(item, fieldName);
        }

        /// <summary>
        /// Provides the ablility to get a link to the target item of a guid type such as droptree in Sitecore.  
        /// /// </summary>
        /// <param name="item">The item that has the droptree.</param>
        /// <param name="fieldName">The name of the drop tree field.</param>
        /// <returns>the link to the item specified in the droptree</returns>
        public static string GetLink(this Item item, string fieldName)
        {
            if (item == null) return String.Empty;
            var targetItem = Sitecore.Context.Database.GetItem(item.Fields[fieldName].Value);
            if (targetItem == null) return String.Empty;
            return LinkManager.GetItemUrl(targetItem);
        }

        /// <summary>
        /// Provides the ablility to get a link to the target item of a guid type such as droptree in Sitecore.  
        /// /// </summary>
        /// <param name="item">The item that has the droptree.</param>
        /// <param name="FieldID">The name of the drop tree field.</param>
        /// <returns>the link to the item specified in the droptree</returns>
        public static string GetLink(this Item item, ID fieldID)
        {
            if (item == null) return String.Empty;
            var targetItem = Sitecore.Context.Database.GetItem(item.Fields[fieldID].Value);
            if (targetItem == null) return String.Empty;
            return LinkManager.GetItemUrl(targetItem);
        }

        /// <summary>
        /// Wraps the link manager call.  
        /// /// </summary>
        /// <param name="item">The item you need a link to.</param>
        /// <returns>the link</returns>
        public static string GetLink(this HtmlHelper htmlHelper, Item item)
        {
            return LinkManager.GetItemUrl(item);
        }


        public static string GetLinkFieldURL(this Item item, LinkField lf)
        {
            switch (lf.LinkType.ToLower())
            {
                case "internal":
                    // Use LinkMananger for internal links, if link is not empty
                    return lf.TargetItem != null ? Sitecore.Links.LinkManager.GetItemUrl(lf.TargetItem) : string.Empty;
                case "media":
                    // Use MediaManager for media links, if link is not empty
                    return lf.TargetItem != null ? Sitecore.Resources.Media.MediaManager.GetMediaUrl(lf.TargetItem) : string.Empty;
                case "external":
                    // Just return external links
                    return lf.Url;
                case "anchor":
                    // Prefix anchor link with # if link if not empty
                    return !string.IsNullOrEmpty(lf.Anchor) ? "#" + lf.Anchor : string.Empty;
                case "mailto":
                    // Just return mailto link
                    return lf.Url;
                case "javascript":
                    // Just return javascript
                    return lf.Url;
                default:
                    // Just please the compiler, this
                    // condition will never be met
                    return lf.Url;
            }
        }


        public static bool IsNullOrEmpty<T>(this IEnumerable<T> enumerable)
        {
            return enumerable == null || !enumerable.Any();
        }

        public static bool IsLast(this IEnumerable<CustomItem> enumerable, CustomItem element)
        {
            return enumerable == null || enumerable.Last().InnerItem.ID == element.InnerItem.ID;
        }

        public static bool IsFirst(this IEnumerable<CustomItem> enumerable, CustomItem element)
        {
            return enumerable == null || enumerable.First().InnerItem.ID == element.InnerItem.ID;
        }

        public static string NormailzeId(this ID id)
        {
            return id.ToGuid().ToString("N").ToLower();
        }

        public static string GetPropertyValue(string Paramater, string DefaultValue)
        {
            string myArgument = string.Empty;

            var rc = Sitecore.Mvc.Presentation.RenderingContext.CurrentOrNull;
            if (rc != null)
            {
                if (rc.Rendering.Parameters.Count() == 0)
                {
                    var renderingParams = Sitecore.Web.WebUtil.ParseUrlParameters(rc.Rendering.RenderingItem.Parameters);
                    myArgument = renderingParams[Paramater];
                }
                else
                {
                    var renderingParams = rc.Rendering.Parameters;
                    myArgument = renderingParams[Paramater];
                }
            }

            if (string.IsNullOrEmpty(myArgument))
            {
                return DefaultValue;
            }
            else
            {
                return myArgument;
            }
        }
    }
}