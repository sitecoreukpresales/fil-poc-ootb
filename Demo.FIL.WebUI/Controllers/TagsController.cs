﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Demo.FIL.WebUI.Models;
using Glass.Mapper;
using Sitecore.Data.Items;
using Glass.Mapper.Sc;
using Sitecore.Data;

namespace Demo.FIL.WebUI.Controllers
{
    public class TagsController : BaseController
    {
        public ActionResult Index()
        {
            //a lot going on here
            //get all the region values added in Sitecore
            //split them and make into a general list
            //then for each item in this list, get the sitecore item

            var contextItemRegion = contextItem["Region"]
                .Split('|', StringSplitOptions.RemoveEmptyEntries)
                .ToList()
                .Select(x => Sitecore.Context.Database.GetItem(x));

            var contextItemTopic = contextItem["Topic"]
                .Split('|', StringSplitOptions.RemoveEmptyEntries)
                .ToList()
                .Select(x => Sitecore.Context.Database.GetItem(x));


            var contextItemContentType = contextItem["ContentType"]
                .Split('|', StringSplitOptions.RemoveEmptyEntries)
                .ToList()
                .Select(x => Sitecore.Context.Database.GetItem(x));

            var tags = new Tags
            {
                RegionTags = new List<Tag>(),
                TopicTags = new List<Tag>(),
                ContentTypeTags = new List<Tag>()
            };

            foreach (var tag in contextItemRegion)
            {
                tags.RegionTags.Add(ss.Cast<Tag>(tag));
            }

            foreach (var tag in contextItemTopic)
            {
                tags.TopicTags.Add(ss.Cast<Tag>(tag));
            }

            foreach (var tag in contextItemContentType)
            {
                tags.ContentTypeTags.Add(ss.Cast<Tag>(tag));
            }

            return View("~/Views/Tags/Tags.cshtml", tags);
        }
    }
}