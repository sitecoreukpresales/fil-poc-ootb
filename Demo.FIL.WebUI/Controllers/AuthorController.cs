﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Demo.FIL.WebUI.Models;
using Glass.Mapper;
using Sitecore.Data.Items;
using Glass.Mapper.Sc;

namespace Demo.FIL.WebUI.Controllers
{
    public class AuthorController : BaseController
    {
        // GET: Author
        public ActionResult Index()
        {
            Item item;

            if (contextItem["Author"].Contains("|"))
            {
                item = contextDb.GetItem(contextItem["Author"].Split('|').ToList().FirstOrDefault());
            }
            else
            {
                item = contextDb.GetItem(contextItem["Author"]);
            }
            

            Item contextItemAuthor = contextDb.GetItem(contextItem["Author"]);
            Author auth = ss.Cast<Author>(item);
            return View("~/Views/Author/Author.cshtml",auth);
        }
    }
}