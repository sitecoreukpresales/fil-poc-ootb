﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Demo.FIL.WebUI.Models;
using Glass.Mapper;
using Sitecore.Data.Items;
using Glass.Mapper.Sc;

namespace Demo.FIL.WebUI.Controllers
{
    public class BaseController : Controller
    {
        public SitecoreService ss;
        public SitecoreContext sc;
        public Sitecore.Data.Database contextDb { get; set; }
        public Item contextItem { get; set; }

        protected BaseController()
        {
            ss = new SitecoreService("web", Context.Default);
            sc= new SitecoreContext();
            contextDb = Sitecore.Context.Database;
            contextItem = Sitecore.Context.Item;
        }
    }
}