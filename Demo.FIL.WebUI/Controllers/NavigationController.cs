﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Demo.FIL.WebUI.Models;

namespace Demo.FIL.WebUI.Controllers
{
    public class NavigationController : BaseController
    {
        // GET: Navigation
        public ActionResult Index()
        {

            Navigation model = sc.GetHomeItem<Navigation>();
            return View("Navigation",model);
        }
    }
}