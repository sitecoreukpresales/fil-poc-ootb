﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Demo.FIL.WebUI.Models;
using Glass.Mapper;
using Sitecore.Data.Items;
using Glass.Mapper.Sc;
using Sitecore.Mvc.Extensions;
using Newtonsoft.Json;

namespace Demo.FIL.WebUI.Controllers
{
    public class HeroListController : BaseController
    {
        // GET: Author
        public ActionResult Index()
        {
            IList<Hero> heros = new List<Hero>();

            const string url = "http://fidelity.demo.lu/sitecore/api/ssc/item/B5E6BD5A-9288-4C27-A8D0-D24BFF34612E/children?fields=Title,Text";

            var a = new WebClient();
            var heroItems = a.DownloadString(url);

            var jsonheros = JsonConvert.DeserializeObject<IList<Hero>>(heroItems);


            return View("~/Views/Components/HeroList.cshtml", jsonheros);
        }
    }
}