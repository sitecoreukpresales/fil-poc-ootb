﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Salesforce.Common;
using Salesforce.Force;
using Sitecore.WFFM.Actions.Base;
using Sitecore.WFFM.Abstractions.Actions;
using System.Threading.Tasks;
using System.Net;
using System.Threading;
using Sitecore.Data;
using System.Globalization;

namespace Demo.FIL.WebUI.Forms.Actions
{
    public class CreateSalesforceActivity : WffmSaveAction
    {
        private static string consumerKey = Sitecore.Configuration.Settings.GetSetting("sfconsumerKey");
        private static string consumerSecret = Sitecore.Configuration.Settings.GetSetting("sfconsumerSecret");
        private static string username = Sitecore.Configuration.Settings.GetSetting("sfuserName");
        private static string password = Sitecore.Configuration.Settings.GetSetting("sfpassword");
        private static string token = Sitecore.Configuration.Settings.GetSetting("sfsecurityToken");
        private static AuthenticationClient auth = new AuthenticationClient();
        private static string contactId = string.Empty;
        private static string activityId = string.Empty;
        private static Contact contact = new Contact();
        private static SFTask task = new SFTask();


        public override void Execute(ID formid, AdaptedResultList adaptedFields, ActionCallContext actionCallContext = null, params object[] data)
        {
            try
            {
                Run(adaptedFields);
                HttpContext.Current.Session["callbackID"] = activityId;

            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Error("Error Is Salesforce Save Action", ex, this);
            }
        }

        private static async Task Run(AdaptedResultList fields)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            var t = auth.UsernamePasswordAsync(consumerKey, consumerSecret, username, password + token);

            var num = 1;
            while (!t.IsCompleted)
            {
                if (num == 20)
                {
                    Sitecore.Diagnostics.Log.Error("Failed to authticate to Salesforce in time", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
                    return;
                }
                Thread.Sleep(200);
                num++;
            }


            if (string.IsNullOrEmpty(auth.AccessToken))
            {
                Sitecore.Diagnostics.Log.Error("Failed to authticate to Salesforce", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
                return;
            }

            contactId = CreateContact(fields);
            Sitecore.Context.User.Profile.SetCustomProperty("SalesforceID", contactId);

            CreateTask(contactId);
        }

        public static string CreateContact(AdaptedResultList fields)
        {

            var client = new ForceClient(auth.InstanceUrl, auth.AccessToken, auth.ApiVersion);
            contact = new Contact() { FirstName = fields.GetEntryByName("First Name").Value, LastName = fields.GetEntryByName("Last Name").Value, Email = fields.GetEntryByName("E-Mail").Value, Phone = fields.GetEntryByName("Telephone").Value, Title = fields.GetEntryByName("Company").Value };


            var t = client.CreateAsync("Contact", contact);
            int num = 1;
            while (!t.IsCompleted)
            {
                Thread.Sleep(500);
                num++;
                if (num == 15)
                {
                    return "";
                }
            }

            contact.Id = t.Result;

            if (contact.Id == null)
            {
                Sitecore.Diagnostics.Log.Error("Unable to create contact in Salesforce", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
                return "";
            }

            return contact.Id;
        }

        private static void CreateTask(String ID)
        {
            var client = new ForceClient(auth.InstanceUrl, auth.AccessToken, auth.ApiVersion);
            task = new SFTask() { WhoId = contact.Id };
            var t = client.CreateAsync("Task", task);
            int num = 1;
            while (!t.IsCompleted)
            {
                Thread.Sleep(200);
                num++;
                if (num == 15)
                {
                    return;
                }
            }

            task.Id = t.Result;

            if (task.Id == null)
            {
                Sitecore.Diagnostics.Log.Error("Unable to create Task in Salesforce", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
                return;
            }
            else
            {
                activityId = task.Id;
                return;
            }
        }



        private class Contact
        {
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string Email { get; set; }
            public string Phone { get; set; }
            public string Title { get; set; }
            public string Id { get; set; }
        }

        private class SFTask
        {
            public string Status = "Not Started";
            public string Description = "Call back requested";
            public string ActivityDate = DateTime.Now.AddDays(1).ToString("yyy-MM-ddTHH:mm:ss", CultureInfo.InvariantCulture);
            public string OwnerId = "00524000000nRqPAAU";
            public string WhoId { get; set; }
            public string Subject = "Call Back Requested";
            public string Id { get; set; }
        }


    }
}